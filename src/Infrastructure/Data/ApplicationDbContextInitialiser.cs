﻿using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using SocialNetwork.Social.Domain.Entities.Feed;

namespace SocialNetwork.Social.Infrastructure.Data;

public static class InitialiserExtensions
{
    public static async Task InitialiseDatabaseAsync(this WebApplication app)
    {
        using var scope = app.Services.CreateScope();

        var initialiser = scope.ServiceProvider.GetRequiredService<ApplicationDbContextInitialiser>();

        await initialiser.InitialiseAsync();

        await initialiser.SeedAsync();
    }
}

public class ApplicationDbContextInitialiser(ILogger<ApplicationDbContextInitialiser> logger, ApplicationDbContext context)
{
    public async Task InitialiseAsync()
    {
        try
        {
            var pendingMigrations = (await context.Database.GetPendingMigrationsAsync()).ToArray();
            if (pendingMigrations.Any())
            {
                logger.LogInformation("Applying migrations: {Migrations}", string.Join(", ", pendingMigrations));
                await context.Database.MigrateAsync();
            }
        }
        catch (Exception ex)
        {
            logger.LogError(ex, "An error occurred while initialising the database");
        }
    }

    public async Task SeedAsync()
    {
        try
        {
            await TrySeedAsync();
        }
        catch (Exception ex)
        {
            logger.LogError(ex, "An error occurred while seeding the database");
        }
    }

    public async Task TrySeedAsync()
    {
        // Default data
        // Seed, if necessary
        if (!context.Posts.Any())
        {
            context.Posts.AddRange(
                new Post
                {
                    Id = Guid.NewGuid(),
                    AuthorId = Guid.Empty,
                    Title = "Welcome!",
                    Content = "Hello, world!",
                    CreatedBy = "system",
                },
                new Post
                {
                    Id = Guid.NewGuid(),
                    AuthorId = Guid.Empty,
                    Title = "Testing Post",
                    Content = "Post 1",
                    CreatedBy = "system",
                },
                new Post
                {
                    Id = Guid.NewGuid(),
                    AuthorId = Guid.Empty,
                    Title = "Testing Post",
                    Content = "Post 2",
                    CreatedBy = "system",
                },
                new Post
                {
                    Id = Guid.NewGuid(),
                    AuthorId = Guid.Empty,
                    Title = "Testing Post",
                    Content = "Post 3",
                    CreatedBy = "system",
                },
                new Post
                {
                    Id = Guid.NewGuid(),
                    AuthorId = Guid.Empty,
                    Title = "Testing Post",
                    Content = "Post 4",
                    CreatedBy = "system",
                },
                new Post
                {
                    Id = Guid.NewGuid(),
                    AuthorId = Guid.Empty,
                    Title = "Testing Post",
                    Content = "Post 5",
                    CreatedBy = "system",
                },
                new Post
                {
                    Id = Guid.NewGuid(),
                    AuthorId = Guid.Empty,
                    Title = "Testing Post",
                    Content = "Post 6",
                    CreatedBy = "system",
                },
                new Post
                {
                    Id = Guid.NewGuid(),
                    AuthorId = Guid.Empty,
                    Title = "Testing Post",
                    Content = "Post 7",
                    CreatedBy = "system",
                },
                new Post
                {
                    Id = Guid.NewGuid(),
                    AuthorId = Guid.Empty,
                    Title = "Testing Post",
                    Content = "Post 8",
                    CreatedBy = "system",
                },
                new Post
                {
                    Id = Guid.NewGuid(),
                    AuthorId = Guid.Empty,
                    Title = "Testing Post",
                    Content = "Post 9",
                    CreatedBy = "system",
                },
                new Post
                {
                    Id = Guid.NewGuid(),
                    AuthorId = Guid.Empty,
                    Title = "Testing Post",
                    Content = "Post 10",
                    CreatedBy = "system",
                }
            );
        }

        await context.SaveChangesAsync();
    }
}
