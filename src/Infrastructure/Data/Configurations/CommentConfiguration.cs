﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SocialNetwork.Social.Domain.Entities.Feed;

namespace SocialNetwork.Social.Infrastructure.Data.Configurations;

public class CommentConfiguration : IEntityTypeConfiguration<Comment>
{
    public void Configure(EntityTypeBuilder<Comment> builder)    
    {
        builder.Property(c => c.Content)
            .HasMaxLength(1000)
            .IsRequired();
    }
}
