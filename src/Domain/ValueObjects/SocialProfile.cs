﻿namespace SocialNetwork.Social.Domain.ValueObjects;

public class SocialProfile : ValueObject
{
    public Guid Id { get; set; }

    public required string Name { get; set; }

    public required string Email { get; set; }

    protected override IEnumerable<object> GetEqualityComponents()
    {
        yield return Id;
        yield return Name;
        yield return Email;
    }
}
