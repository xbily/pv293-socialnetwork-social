﻿namespace SocialNetwork.Social.Domain.Events.Feed;

public class PostLikedEvent(Guid postId, Guid userId) : BaseEvent
{
    public Guid PostId { get; } = postId;

    public Guid UserId { get; } = userId;
}
