﻿using SocialNetwork.Social.Domain.Entities.Feed;

namespace SocialNetwork.Social.Domain.Events.Feed;

public class PostCommentedEvent(Post post, Comment comment) : BaseEvent
{
    public Post Post { get; } = post;
    public Comment Comment { get; } = comment;
}
