﻿using SocialNetwork.Social.Domain.Entities.Feed;

namespace SocialNetwork.Social.Domain.Events.Feed;

public class PostPublishedEvent(Post post) : BaseEvent
{
    public Post Post { get; } = post;
}
