﻿namespace SocialNetwork.Social.Domain.Enums;

public enum ProfilePlan
{
    Basic = 0,
    Premium = 1,
}
