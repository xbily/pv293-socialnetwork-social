﻿using MediatR;

namespace SocialNetwork.Social.Domain.Common;

public abstract class BaseEvent : INotification;
