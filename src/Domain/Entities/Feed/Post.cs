﻿namespace SocialNetwork.Social.Domain.Entities.Feed;

public class Post : BaseAuditableEntity
{
    public Guid AuthorId { get; set; }

    public string Title { get; set; } = null!;
    public string Content { get; set; } = null!;

    public ulong LikesCount { get; set; } = 0;

    public virtual List<Comment> Comments { get; set; } = new();
}
