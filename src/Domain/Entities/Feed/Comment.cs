namespace SocialNetwork.Social.Domain.Entities.Feed;

public class Comment : BaseAuditableEntity
{
    public Guid AuthorId { get; set; }
    public Guid PostId { get; set; }
    public string Content { get; set; } = null!;
}
