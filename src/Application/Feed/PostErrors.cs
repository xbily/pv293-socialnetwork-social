﻿using ErrorOr;

namespace SocialNetwork.Social.Application.Feed;

public static class PostErrors
{
    public static Error PostNotFound(Guid postId) =>
        Error.NotFound("PostNotFound", $"Post with id: {postId} not found");
}
