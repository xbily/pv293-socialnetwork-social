﻿using ErrorOr;
using MediatR;
using SocialNetwork.Social.Application.Common.Interfaces;
using SocialNetwork.Social.Domain.Entities.Feed;

namespace SocialNetwork.Social.Application.Feed.Queries;

public record GetPostByIdQuery(Guid PostId) : IRequest<ErrorOr<Post>>;

public class GetPostByIdQueryHandler(IApplicationDbContext dbContext) : IRequestHandler<GetPostByIdQuery, ErrorOr<Post>>
{
    public async Task<ErrorOr<Post>> Handle(GetPostByIdQuery request, CancellationToken cancellationToken)
    {
        var postIdToFind = request.PostId;
        var post = await dbContext
            .Posts
            .FindAsync(new object?[] {postIdToFind}, cancellationToken: cancellationToken);

        if (post is null) return PostErrors.PostNotFound(postIdToFind);

        return post;
    }
}
