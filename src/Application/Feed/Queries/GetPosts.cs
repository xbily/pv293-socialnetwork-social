﻿using MediatR;
using SocialNetwork.Social.Application.Common.Interfaces;
using SocialNetwork.Social.Domain.Entities.Feed;

namespace SocialNetwork.Social.Application.Feed.Queries;

public class GetPostsQuery : IRequest<List<Post>>; // Could include a filter and pagination here

public class GetPostsQueryHandler(IApplicationDbContext dbContext) : IRequestHandler<GetPostsQuery, List<Post>>
{
    public Task<List<Post>> Handle(GetPostsQuery request, CancellationToken cancellationToken) =>
        dbContext
            .Posts
            .AsNoTracking()
            .ToListAsync(cancellationToken: cancellationToken);
}
