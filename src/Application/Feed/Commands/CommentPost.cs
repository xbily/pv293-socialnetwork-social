﻿using ErrorOr;
using MediatR;
using SocialNetwork.Social.Application.Common.Interfaces;
using SocialNetwork.Social.Domain.Entities.Feed;
using SocialNetwork.Social.Domain.Events.Feed;

namespace SocialNetwork.Social.Application.Feed.Commands;

public record CommentPostCommand(Guid PostId, Guid CommenterId, string Content) : IRequest<ErrorOr<Comment>>;

public class CommentPostCommandHandler(IApplicationDbContext dbContext) : IRequestHandler<CommentPostCommand, ErrorOr<Comment>>
{
    public async Task<ErrorOr<Comment>> Handle(CommentPostCommand request, CancellationToken cancellationToken)
    {
        var postId = request.PostId;
        var post = await dbContext.Posts.FindAsync([postId], cancellationToken: cancellationToken);
        if (post is null) return PostErrors.PostNotFound(postId);

        var comment = new Comment {AuthorId = request.CommenterId, Content = request.Content, PostId = request.PostId,};

        await dbContext.Comments.AddAsync(comment, cancellationToken);
        post.Comments.Add(comment);
        await dbContext.SaveChangesAsync(cancellationToken);

        post.AddDomainEvent(new PostCommentedEvent(post, comment));
        dbContext.Posts.Update(post);
        await dbContext.SaveChangesAsync(cancellationToken);

        return comment;
    }
}

public class CommentPostCommandValidator : AbstractValidator<CommentPostCommand>
{
    public CommentPostCommandValidator()
    {
        RuleFor(p => p.PostId)
            .NotEmpty();

        RuleFor(p => p.CommenterId)
            .NotEmpty();

        RuleFor(p => p.Content)
            .NotEmpty()
            .Length(3, 512);
    }
}
