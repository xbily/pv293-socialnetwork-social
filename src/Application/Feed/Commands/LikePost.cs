﻿using ErrorOr;
using MediatR;
using SocialNetwork.Social.Application.Common.Interfaces;
using SocialNetwork.Social.Domain.Events.Feed;

namespace SocialNetwork.Social.Application.Feed.Commands;

public record LikePostCommand(Guid PostId, Guid LikerId) : IRequest<ErrorOr<Success>>;

public class LikePostCommandHandler(IApplicationDbContext dbContext) : IRequestHandler<LikePostCommand, ErrorOr<Success>>
{
    public async Task<ErrorOr<Success>> Handle(LikePostCommand request, CancellationToken cancellationToken)
    {
        var requestPostId = request.PostId;
        var post = await dbContext.Posts.FindAsync(requestPostId);
        if (post is null) return PostErrors.PostNotFound(requestPostId);

        post.LikesCount++; // I would create a whole table for likers and postId to add who liked the post

        post.AddDomainEvent(new PostLikedEvent(post.Id, request.LikerId));

        await dbContext.SaveChangesAsync(cancellationToken);
        return Result.Success;
    }
}
