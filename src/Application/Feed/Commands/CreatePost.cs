﻿using ErrorOr;
using MediatR;
using SocialNetwork.Social.Application.Common.Interfaces;
using SocialNetwork.Social.Domain.Entities.Feed;
using SocialNetwork.Social.Domain.Events.Feed;

namespace SocialNetwork.Social.Application.Feed.Commands;

public record PublishPostCommand(Guid AuthorId, string Title, string Content) : IRequest<ErrorOr<Post>>;

public class PublishPostCommandHandler(IApplicationDbContext dbContext) : IRequestHandler<PublishPostCommand, ErrorOr<Post>>
{
    public async Task<ErrorOr<Post>> Handle(PublishPostCommand request, CancellationToken cancellationToken)
    {
        var post = new Post {AuthorId = request.AuthorId, Title = request.Title, Content = request.Content};

        post.AddDomainEvent(new PostPublishedEvent(post));

        await dbContext.Posts.AddAsync(post, cancellationToken);

        await dbContext.SaveChangesAsync(cancellationToken);

        return post;
    }
}

public class PublishPostCommandValidator : AbstractValidator<PublishPostCommand>
{
    public PublishPostCommandValidator()
    {
        RuleFor(p => p.AuthorId)
            .NotEmpty();

        RuleFor(p => p.Title)
            .NotEmpty()
            .Length(3, 64);

        RuleFor(p => p.Content)
            .NotEmpty()
            .Length(3, 512);
    }
}
