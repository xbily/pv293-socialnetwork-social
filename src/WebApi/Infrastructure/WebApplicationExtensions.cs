﻿using WebApi.Endpoints;

namespace WebApi.Infrastructure;

public static class WebApplicationExtensions
{
    public static RouteGroupBuilder MapEndpoints(this RouteGroupBuilder routeBuilder) =>
        MapPostEndpoints(routeBuilder);

    private static RouteGroupBuilder MapPostEndpoints(RouteGroupBuilder routeBuilder)
    {
        var postGroup = routeBuilder.MapGroup("posts");
        new FeedEndpoints().Map(postGroup);

        return routeBuilder;
    }
}
