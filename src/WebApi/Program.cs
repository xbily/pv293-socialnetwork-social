using SocialNetwork.Social.Application;
using SocialNetwork.Social.Infrastructure;
using SocialNetwork.Social.Infrastructure.Data;
using WebApi;
using WebApi.Infrastructure;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddApplicationServices();
builder.Services.AddInfrastructureServices(builder.Configuration);
builder.Services.AddWebServices();

var app = builder.Build();

app.UseSwagger();
if (app.Environment.IsDevelopment())
{
    app.UseSwaggerUI();
    await app.InitialiseDatabaseAsync();
}

app.UseHealthChecks("/health");
app.UseHttpsRedirection();

app.Map("/", () => Results.Redirect("/api"));

app.MapGroup("api")
    .AllowAnonymous()
    .MapEndpoints()
    .WithOpenApi();

app.Run();
