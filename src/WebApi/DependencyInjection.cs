﻿using SocialNetwork.Social.Infrastructure.Data;

namespace WebApi;

public static class DependencyInjection
{
    public static IServiceCollection AddWebServices(this IServiceCollection services)
    {
        services.AddHealthChecks()
            .AddDbContextCheck<ApplicationDbContext>();

        services.AddEndpointsApiExplorer();
        services.AddSwaggerGen(options => { });

        return services;
    }
}
