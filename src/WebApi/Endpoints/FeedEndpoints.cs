﻿using ErrorOr;
using MediatR;
using SocialNetwork.Social.Application.Feed.Commands;
using SocialNetwork.Social.Application.Feed.Queries;

namespace WebApi.Endpoints;

public class FeedEndpoints
{
    public void Map(IEndpointRouteBuilder builder)
    {
        builder.MapGet("", GetAllPosts);

        builder.MapGet("/{postId:guid}", GetPostById);

        builder.MapPost("", CreatePost);

        builder.MapPost("/{postId:guid}/comments", CommentPost);

        builder.MapPost("/{postId:guid}/like", LikePost);
    }

    private async Task<IResult> GetAllPosts(ISender sender) =>
        Results.Ok(await sender.Send(new GetPostsQuery()));

    private async Task<IResult> GetPostById(Guid postId, ISender sender)
    {
        var errorOrPost = await sender.Send(new GetPostByIdQuery(postId));
        return errorOrPost.MatchFirst(
            Results.Ok,
            error => error.Type switch
            {
                ErrorType.NotFound => Results.NotFound(error.Description),
                _ => Results.BadRequest(error.Description)
            }
        );
    }

    private async Task<IResult> CreatePost(PublishPostCommand command, ISender sender)
    {
        var errorOrCreatedPost = await sender.Send(command);
        return errorOrCreatedPost.MatchFirst(
            createdPost => Results.Created($"/api/posts/{createdPost.Id}", createdPost),
            error => error.Type switch
            {
                ErrorType.Validation => Results.BadRequest(error.Description),
                ErrorType.Failure => Results.Problem(error.Description),
                _ => Results.BadRequest(error.Description)
            }
        );
    }

    private async Task<IResult> CommentPost(Guid postId, CommentPostCommand command, ISender sender)
    {
        var errorOrComment = await sender.Send(command with {PostId = postId});
        return errorOrComment.MatchFirst(
            comment => Results.Created($"api/posts/{postId}", comment),
            error => error.Type switch
            {
                ErrorType.Validation => Results.BadRequest(error.Description),
                ErrorType.NotFound => Results.NotFound(error.Description),
                _ => Results.BadRequest(error.Description)
            }
        );
    }

    private async Task<IResult> LikePost(Guid postId, LikePostCommand command, ISender sender)
    {
        var errorOrSuccess = await sender.Send(command with {PostId = postId});
        return errorOrSuccess.MatchFirst(
            _ => Results.Ok(),
            error => error.Type switch
            {
                ErrorType.Validation => Results.BadRequest(error.Description),
                ErrorType.NotFound => Results.NotFound(error.Description),
                _ => Results.BadRequest(error.Description)
            }
        );
    }
}
